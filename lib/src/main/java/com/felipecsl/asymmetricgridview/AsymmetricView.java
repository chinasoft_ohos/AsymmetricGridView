/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipecsl.asymmetricgridview;

import ohos.agp.components.Component;

/**
 * 不规则网格控件
 *
 * @since 2021-04-02
 */
interface AsymmetricView {
    /**
     * 是否debug
     *
     * @return debug
     */
    boolean isDebugging();

    /**
     * 列数
     *
     * @return 列数
     */
    int getNumColumns();

    /**
     * 是否允许重新排列
     *
     * @return 可否重新排列
     */
    boolean isAllowReordering();

    /**
     * 触发item点击事件
     *
     * @param index 位置
     * @param component 控件
     */
    void fireOnItemClick(int index, Component component);

    /**
     * 触发item长按点击事件
     *
     * @param index 位置
     * @param component 控件
     * @return return
     */
    boolean fireOnItemLongClick(int index, Component component);

    /**
     * 获取列宽
     *
     * @return 列宽
     */
    int getColumnWidth();

    /**
     * 获取分隔线高度
     *
     * @return debug
     */
    int getDividerHeight();

    /**
     * 获取请求的水平间距
     *
     * @return 水平间距
     */
    int getRequestedHorizontalSpacing();
}
