/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipecsl.asymmetricgridview;

import ohos.agp.components.ComponentContainer;
import ohos.agp.database.DataSetSubscriber;
import ohos.app.Context;

/**
 * 不规则网格控件
 *
 * @param <T> BaseViewHolder
 * @since 2021-04-02
 */
public final class AsymmetricRecyclerViewAdapter<T extends BaseViewHolder>
    extends AsyBaseAdapter<AdapterImpl.ViewHolder> implements AgvBaseAdapter<T> {
    private final AsymmetricRecyclerView recycleView;
    private final AgvRecyclerViewAdapter<T> wrappedAdapter;
    private final AdapterImpl adapterImpl;

    /**
     * 构造方法
     *
     * @param context 上下文
     * @param recyclerView 不规则网格控件
     * @param wrappedAdapter 适配器
     */
    public AsymmetricRecyclerViewAdapter(Context context, AsymmetricRecyclerView recyclerView,
                                         AgvRecyclerViewAdapter<T> wrappedAdapter) {
        this.recycleView = recyclerView;
        this.wrappedAdapter = wrappedAdapter;
        this.adapterImpl = new AdapterImpl(context, this, recyclerView);
        wrappedAdapter.addDataSubscriber(new DataSetSubscriber() {
            @Override
            public void onChanged() {
                recalculateItemsPerRow();
            }
        });
    }

    @Override
    public int getCount() {
        return adapterImpl.getRowCount();
    }

    @Override
    public int getActualItemCount() {
        return wrappedAdapter.getCount();
    }

    @Override
    public AsymmetricItem getItem(int position) {
        return wrappedAdapter.getItem(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public AdapterImpl.ViewHolder onCreateViewHolder(ComponentContainer parent, int viewType) {
        return adapterImpl.onCreateViewHolder();
    }

    @Override
    public void onBindViewHolder(AdapterImpl.ViewHolder holder, int position) {
        adapterImpl.onBindViewHolder(holder, position, recycleView);
    }

    @Override
    public void notifyDataSetChanged() {
        notifyDataChanged();
    }

    @Override
    public AsymmetricViewHolder<T> onCreateAsymmetricViewHolder(
        int position, ComponentContainer parent, int viewType) {
        return new AsymmetricViewHolder(wrappedAdapter.onCreateViewHolder(parent, viewType));
    }

    @Override
    public void onBindAsymmetricViewHolder(AsymmetricViewHolder<T> holder, ComponentContainer parent, int position) {
        wrappedAdapter.onBindViewHolder(holder.wrappedViewHolder, position);
    }

    @Override
    public int getItemViewType(int position) {
        return wrappedAdapter.getItemComponentType(position);
    }

    void recalculateItemsPerRow() {
        adapterImpl.recalculateItemsPerRow();
    }
}
