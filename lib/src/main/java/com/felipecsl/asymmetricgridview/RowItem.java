/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipecsl.asymmetricgridview;

/**
 * 行信息类
 *
 * @since 2021-04-02
 */
final class RowItem {
    private final AsymmetricItem item;
    private final int index;

    /**
     * 构造方法
     *
     * @param index 位置
     * @param item 不规则对象
     */
    RowItem(int index, AsymmetricItem item) {
        this.index = index;
        this.item = item;
    }

    /**
     * 获取不规则对象
     *
     * @return return
     */
    AsymmetricItem getItem() {
        return item;
    }

    /**
     * 获取位置
     *
     * @return 位置
     */
    int getIndex() {
        return index;
    }
}
