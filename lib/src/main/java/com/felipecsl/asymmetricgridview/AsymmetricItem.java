/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipecsl.asymmetricgridview;

import ohos.utils.Sequenceable;

/**
 * 不规则对象
 *
 * @since 2021-04-02
 */
public interface AsymmetricItem extends Sequenceable {
    /**
     * 列跨度
     *
     * @return 跨度
     */
    int getColumnSpan();

    /**
     * 行跨度
     *
     * @return 跨度
     */
    int getRowSpan();
}
