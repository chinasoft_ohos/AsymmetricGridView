/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipecsl.asymmetricgridview;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

import java.util.ArrayList;
import java.util.List;

/**
 * 行信息
 *
 * @since 2021-04-02
 */
class RowInfo implements Sequenceable {
    private final List<RowItem> items;
    private final int rowHeight;
    private final float spaceLeft;

    /**
     * 构造方法
     *
     * @param rowHeight 行高
     * @param items 行数据集合
     * @param spaceLeft 剩余空间
     */
    protected RowInfo(int rowHeight, List<RowItem> items, float spaceLeft) {
        this.rowHeight = rowHeight;
        this.items = items;
        this.spaceLeft = spaceLeft;
    }

    /**
     * 序列化构造
     *
     * @param in
     */
    protected RowInfo(final Parcel in) {
        rowHeight = in.readInt();
        spaceLeft = in.readFloat();
        int totalItems = in.readInt();

        items = new ArrayList<>();
        final ClassLoader classLoader = AsymmetricItem.class.getClassLoader();

        for (int i = 0; i < totalItems; i++) {
            items.add(new RowItem(in.readInt(), (AsymmetricItem) in.readParcelableEx(classLoader)));
        }
    }

    public List<RowItem> getItems() {
        return items;
    }

    public int getRowHeight() {
        return rowHeight;
    }

    public float getSpaceLeft() {
        return spaceLeft;
    }

    @Override
    public boolean marshalling(Parcel parcel) {
        parcel.writeInt(rowHeight);
        parcel.writeFloat(spaceLeft);
        parcel.writeInt(items.size());

        for (RowItem rowItem : items) {
            parcel.writeInt(rowItem.getIndex());
            parcel.writeSequenceable(rowItem.getItem());
        }
        return false;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        parcel.writeInt(rowHeight);
        parcel.writeFloat(spaceLeft);
        parcel.writeInt(items.size());

        for (RowItem rowItem : items) {
            parcel.writeInt(rowItem.getIndex());
            parcel.writeSequenceable(rowItem.getItem());
        }
        return false;
    }
}
