/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipecsl.asymmetricgridview;

import ohos.agp.components.ComponentContainer;

/**
 * 适配器接口
 *
 * @param <T> BaseViewHolder
 * @since 2021-04-02
 */
interface AgvBaseAdapter<T extends BaseViewHolder> {
    int getActualItemCount();

    AsymmetricItem getItem(int position);

    void notifyDataSetChanged();

    int getItemViewType(int actualIndex);

    AsymmetricViewHolder<T> onCreateAsymmetricViewHolder(int position, ComponentContainer parent, int viewType);

    void onBindAsymmetricViewHolder(AsymmetricViewHolder<T> holder, ComponentContainer parent, int position);
}
