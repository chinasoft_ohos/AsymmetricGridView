/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipecsl.asymmetricgridview;

import ohos.app.Context;

/**
 * 不规则布局实现类
 *
 * @since 2021-04-02
 */
final class AsymmetricViewImpl {
    private static final int DEFAULT_COLUMN_COUNT = 3;
    protected int numColumns = DEFAULT_COLUMN_COUNT;
    protected int requestedHorizontalSpacing;
    protected int requestedColumnWidth;
    protected int requestedColumnCount;
    protected boolean isAllowReordering;
    protected boolean isDebugging;
    private final int spacingWidth = 5;

    /**
     * 构造方法
     *
     * @param context 上下文
     */
    AsymmetricViewImpl(Context context) {
        requestedHorizontalSpacing = Utils.vpToPx(context, spacingWidth);
    }

    /**
     * 设置请求的列宽
     *
     * @param width 宽度
     */
    public void setRequestedColumnWidth(int width) {
        requestedColumnWidth = width;
    }

    /**
     * 设置请求的列数
     *
     * @param requestedColumnCount 列数
     */
    public void setRequestedColumnCount(int requestedColumnCount) {
        this.requestedColumnCount = requestedColumnCount;
    }

    /**
     * 获取请求的水平间距
     *
     * @return 水平间距
     */
    public int getRequestedHorizontalSpacing() {
        return requestedHorizontalSpacing;
    }

    /**
     * 设置请求的水平间距
     *
     * @param spacing 水平间距
     */
    public void setRequestedHorizontalSpacing(int spacing) {
        requestedHorizontalSpacing = spacing;
    }

    /**
     * 确定列数处理
     *
     * @param availableSpace 列数
     * @return return
     */
    public int determineColumns(int availableSpace) {
        this.numColumns = 0;
        if (requestedColumnWidth > 0) {
            numColumns = (availableSpace + requestedHorizontalSpacing)
                / (requestedColumnWidth + requestedHorizontalSpacing);
        } else if (requestedColumnCount > 0) {
            numColumns = requestedColumnCount;
        } else {
            // Default to 2 columns
            numColumns = DEFAULT_COLUMN_COUNT;
        }
        if (numColumns <= 0) {
            numColumns = 1;
        }
        return numColumns;
    }

    /**
     * 获取列数
     *
     * @return 列数
     */
    public int getNumColumns() {
        return numColumns;
    }

    /**
     * 获取列可用空间
     *
     * @param availableSpace 可用空间
     * @return return
     */
    public int getColumnWidth(int availableSpace) {
        return (availableSpace - ((numColumns - 1) * requestedHorizontalSpacing)) / numColumns;
    }

    /**
     * 是否允许重新排序
     *
     * @return 可否重新排列
     */
    public boolean isAllowReordering() {
        return isAllowReordering;
    }

    /**
     * 设置可否重新排列
     *
     * @param isReordering 是否重新排列
     */
    public void setAllowReordering(boolean isReordering) {
        this.isAllowReordering = isReordering;
    }

    /**
     * 调试状态
     *
     * @return 是否调试
     */
    public boolean isDebugging() {
        return isDebugging;
    }

    /**
     * 调试设置
     *
     * @param isDebug 调试开关
     */
    public void setDebugging(boolean isDebug) {
        this.isDebugging = isDebug;
    }
}