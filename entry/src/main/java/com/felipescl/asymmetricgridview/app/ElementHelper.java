/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipescl.asymmetricgridview.app;

import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * Element 工具类
 *
 * @since 2021-04-02
 */
public class ElementHelper {
    private ElementHelper() {
    }

    /**
     * 获取色值
     *
     * @param context
     * @param resourceId
     * @return 色值
     */
    public static Color getColor(Context context, int resourceId) {
        try {
            int color = context.getResourceManager().getElement(resourceId).getColor();
            return new Color(color);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.getMessage();
        }
        return new Color();
    }
}
