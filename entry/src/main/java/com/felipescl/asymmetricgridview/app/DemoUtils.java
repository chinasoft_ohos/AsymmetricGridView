/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipescl.asymmetricgridview.app;

import java.util.ArrayList;
import java.util.List;

/**
 * 随机数据生成类
 *
 * @since 2021-04-02
 */
public class DemoUtils {
    private int currentOffset;
    private final float max = 0.2f;
    private final int maxSpan = 2;

    /**
     * 构造方法
     */
    public DemoUtils() {
    }

    /**
     * 设置当前位置
     *
     * @param current 位置
     */
    public void setCurrentOffset(int current) {
        currentOffset = current;
    }

    /**
     * 生成随机数
     *
     * @param qty 生成的数据个数
     * @return 数据集合
     */
    public List<DemoItem> moarItems(int qty) {
        List<DemoItem> items = new ArrayList<>();
        for (int i = 0; i < qty; i++) {
            int colSpan = Math.random() < max ? maxSpan : 1;
            int rowSpan = colSpan;
            DemoItem item = new DemoItem(colSpan, rowSpan, currentOffset + i);
            items.add(item);
        }
        currentOffset += qty;
        return items;
    }
}
