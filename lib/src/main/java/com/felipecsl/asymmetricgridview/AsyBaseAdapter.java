/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipecsl.asymmetricgridview;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

/**
 * 不规则适配器基类
 *
 * @param <VH> BaseViewHolder
 * @since 2021-04-02
 */
public abstract class AsyBaseAdapter<VH extends BaseViewHolder> extends BaseItemProvider {
    /**
     * 创建ViewHolder
     *
     * @param componentContainer 控件
     * @param viewType 控件类型
     * @return BaseViewHolder 控件持有者
     */
    public abstract VH onCreateViewHolder(ComponentContainer componentContainer, int viewType);

    /**
     * 数据绑定
     *
     * @param holder 控件持有者
     * @param position 位置
     */
    public abstract void onBindViewHolder(VH holder, int position);

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component itemComponent = component;
        VH itemViewHolder;
        if (itemComponent == null) {
            itemViewHolder = onCreateViewHolder(componentContainer, getItemComponentType(position));
            itemViewHolder.itemComponent.setTag(itemViewHolder);
        } else {
            itemViewHolder = (VH) itemComponent.getTag();
        }
        onBindViewHolder(itemViewHolder, position);
        return itemViewHolder.itemComponent;
    }
}
