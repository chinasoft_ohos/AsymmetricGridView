/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipecsl.asymmetricgridview;

import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

/**
 * 常用工具类
 *
 * @since 2021-04-02
 */
public final class Utils {
    private Utils() {
    }

    /**
     * vp转像素
     *
     * @param context 上下文
     * @param vp vp值
     * @return px 像素值
     */
    public static int vpToPx(Context context, float vp) {
        return DisplayManager.getInstance().getDefaultDisplay(context)
            .map(display -> (int) (display.getAttributes().densityPixels * vp))
            .orElse(0);
    }

    static int getScreenWidth(final Context context) {
        if (context == null) {
            return 0;
        }
        return (int) getDisplayMetrics(context).orElse(new Point()).getPointX();
    }

    /**
     * Returns a valid DisplayMetrics object
     *
     * @param context valid context
     * @return DisplayMetrics object
     */
    static Optional<Point> getDisplayMetrics(Context context) {
        return DisplayManager.getInstance().getDefaultDisplay(context)
            .map(display -> {
                Point point = new Point();
                display.getSize(point);
                return Optional.of(point);
            }).orElse(null);
    }
}
