/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipecsl.asymmetricgridview;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * 线程切换帮助类
 *
 * @since 2021-01-27
 */
public final class HiExecutor {
    private HiExecutor() {
    }

    /**
     * 切换任务到主线程执行
     *
     * @param runnable 执行的runnable
     */
    public static void runMainThread(Runnable runnable) {
        EventRunner eventRunner = EventRunner.getMainEventRunner();
        EventHandler eventHandler = new EventHandler(eventRunner);
        eventHandler.postSyncTask(runnable);
    }

    /**
     * 在子线程执行任务
     *
     * @param runnable 执行的runnable
     */
    public static void runWorkThread(Runnable runnable) {
        EventRunner eventRunner = EventRunner.create(true);
        if (eventRunner == null) {
            return;
        }
        EventHandler eventHandler = new EventHandler(eventRunner);
        eventHandler.postTask(runnable, 0, EventHandler.Priority.IMMEDIATE);
    }
}
