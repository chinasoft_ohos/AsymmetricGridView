/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.felipescl.asymmetricgridview.app;

import ohos.utils.Parcel;

import com.felipecsl.asymmetricgridview.AsymmetricItem;

import java.util.Locale;

/**
 * 主界面
 *
 * @since 2021-04-02
 */
public class DemoItem implements AsymmetricItem {
    private int columnSpan;
    private int rowSpan;
    private int position;

    /**
     * 构造方法
     */
    public DemoItem() {
        this(1, 1, 0);
    }

    /**
     * 构造方法
     *
     * @param columnSpan
     * @param rowSpan
     * @param position
     */
    public DemoItem(int columnSpan, int rowSpan, int position) {
        this.columnSpan = columnSpan;
        this.rowSpan = rowSpan;
        this.position = position;
    }

    /**
     * 构造方法
     *
     * @param in
     */
    public DemoItem(Parcel in) {
        readFromParcel(in);
    }

    private void readFromParcel(Parcel in) {
        columnSpan = in.readInt();
        rowSpan = in.readInt();
        position = in.readInt();
    }

    @Override
    public int getColumnSpan() {
        return columnSpan;
    }

    @Override
    public int getRowSpan() {
        return rowSpan;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return String.format(Locale.ROOT, "%s: %sx%s", position, rowSpan, columnSpan);
    }

    @Override
    public boolean marshalling(Parcel parcel) {
        parcel.writeInt(columnSpan);
        parcel.writeInt(rowSpan);
        parcel.writeInt(position);
        return false;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        columnSpan = parcel.readInt();
        rowSpan = parcel.readInt();
        position = parcel.readInt();
        return false;
    }
}
