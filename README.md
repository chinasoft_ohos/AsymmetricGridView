# AsymmetricGridView


#### 项目介绍
- 项目名称：AsymmetricGridView
- 所属系列：openharmony的第三方组件适配移植
- 功能：非对称网格视图
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：master分支

#### 效果演示
  ![image1](img/shots.gif)

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:AsymmetricGridview:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

In your layout xml:

    <com.felipecsl.lib.AsymmetricRecyclerView
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        ohos:id="$+id:recyclerView"
        ohos:height="match_parent"
        ohos:width="match_parent"/>

In your activity class:

        @Override
        public void onStart(Intent intent) {
            super.onStart(intent);
            super.setUIContent(ResourceTable.Layout_ability_main);
            AsymmetricRecyclerView recyclerView = (AsymmetricRecyclerView) findComponentById(ResourceTable.Id_recyclerView);
            RecyclerViewAdapter adapter = new RecyclerViewAdapter(demoUtils.moarItems(50));
            recyclerView.setRequestedColumnCount(3);
            recyclerView.setDebugging(true);
            recyclerView.setRequestedHorizontalSpacing(Utils.vpToPx(this, 3));
            recyclerView.setItemProvider(new AsymmetricRecyclerViewAdapter<>(this, recyclerView, adapter));
        }

Whenever your adapter changes (add or remove items), the grid will automatically reflect those
changes by subscribing to your adapter changes.

Toggle to enable/disable reordering of elements to better fill the grid

    // Setting to true will move items up and down to better use the space
    // Defaults to false.
    listView.setAllowReordering(true);
    listView.isAllowReordering(); // true

#### 测试信息

CodeCheck代码测试无异常
``
CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异  


#### 版本迭代

- 1.0.0

#### 版权和许可信息

    The MIT License (MIT)
    
    Copyright (c) 2011-2014 Felipe Lima
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
